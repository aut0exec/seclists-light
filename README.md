![seclists.png](SecLists.png "seclists.png")

## About SecLists-lite

Seclists-lite is a drastically smaller collection of lists from the larger SecLists project. The intention is
to provide testers with a small set of lists as well as help save disk space.

This project is maintained by [aut0exec](https://gitlab.com/aut0exec) for Parrot OS.

### About the Full SecLists

SecLists is the security tester's companion. It's a collection of multiple types of lists used during security assessments, collected in one place. List types include usernames, passwords, URLs, sensitive data patterns, fuzzing payloads, web shells, and many more. The goal is to enable a security tester to pull this repository onto a new testing box and have access to every type of list that may be needed.

The SecLists project is maintained by [Daniel Miessler](https://danielmiessler.com/), [Jason Haddix](https://twitter.com/Jhaddix), and [g0tmi1k](https://blog.g0tmi1k.com/).

- - -

<!--- details anchor -->

### Repository details

Size of a complete clone of SecLists is currently at `1.4 GB` however seclists-lite is roughly `400 MB`

Cloning this repository should take 1-2 minutes at 5MB/s speeds.

<!--- details anchor -->

- - -

### Install

**Zip**

*Not currently implemented but how to eventually*

```
wget -c https://gitlab.com/aut0exec/seclists-lite/archive/master.zip -O seclist-lite.zip \
  && unzip seclist-lite.zip \
  && rm -f seclist-lite.zip
```

**Git (Complete)**

```
git clone https://gitlab.com/aut0exec/seclists-lite.git
```

**Parrot Linux**

```
apt -y install seclists-lite
```

- - -

### Attribution

See [CONTRIBUTORS.md](CONTRIBUTORS.md)

- - -

### Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

- - -

### Similar Projects

- [Assetnote Wordlists](https://wordlists.assetnote.io/)
- [fuzz.txt](https://github.com/Bo0oM/fuzz.txt)
- [FuzzDB](https://github.com/fuzzdb-project/fuzzdb)
- [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings)
- [Cook](https://github.com/giteshnxtlvl/cook)
- [SamLists](https://github.com/the-xentropy/samlists)

- - -

### Licensing

This project is licensed under the [MIT license](LICENSE).

[![MIT License](https://img.shields.io/badge/license-MIT_License-blue)](https://opensource.org/licenses/MIT)
—

<sup>NOTE: Downloading this repository is likely to cause a false-positive alarm by your anti-virus or anti-malware software, the filepath should be whitelisted. There is nothing in SecLists that can harm your computer as-is, however it's not recommended to store these files on a server or other important system due to the risk of local file include attacks.</sup>
